#include <stdio.h>

int main() {
    double nilai, quiz, absen, uts, uas, tugas;
    char Huruf_Mutu;

    printf("Masukkan nilai absen: ");
    scanf("%lf", &absen);

    printf("Masukkan nilai tugas: ");
    scanf("%lf", &tugas);

    printf("Masukkan nilai quiz: ");
    scanf("%lf", &quiz);

    printf("Masukkan nilai UTS: ");
    scanf("%lf", &uts);

    printf("Masukkan nilai UAS: ");
    scanf("%lf", &uas);

    printf("Absen = %.2f UTS = %.2f\n", absen, uts);
    printf("Tugas = %.2f UAS = %.2f\n", tugas, uas);
    printf("Quiz = %.2f\n", quiz);

    nilai = ((0.1 * absen) + (0.2 * tugas) + (0.3 * quiz) + (0.4 * uts) + (0.5 * uas)) / 2;

    if (nilai > 85 && nilai <= 100)
        Huruf_Mutu = 'A';
    else if (nilai > 70 && nilai <= 85)
        Huruf_Mutu = 'B';
    else if (nilai > 55 && nilai <= 70)
        Huruf_Mutu = 'C';
    else if (nilai > 40 && nilai <= 55)
        Huruf_Mutu = 'D';
    else if (nilai >= 0 && nilai <= 40)
        Huruf_Mutu = 'E';

    printf("Huruf Mutu : %c\n", Huruf_Mutu);

    return 0;
}
